//
//  NBCompassView.m
//  CustomView
//
//  Created by Nordin on 27/02/14.
//  Copyright (c) 2014 Fennan Digital. All rights reserved.
//

#import "NBCompassView.h"

@implementation NBCompassView {
    CGFloat testDirection;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    testDirection = 0.25*M_PI;
    
    // source: http://www.techotopia.com/index.php/An_iOS_7_Graphics_Tutorial_using_Core_Graphics_and_Core_Image
    // First, get the drawing context (read as a pencil/brush/... to draw)
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.0);
    
    // Let's draw a circle in the centre of the screen
    //CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGRect screenRect = CGRectApplyAffineTransform(self.bounds, self.transform);
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    CGFloat width = screenWidth;// - 20.0;
    // CGFloat height = screenWidth - 40.0;
    CGFloat radius = width/4;
    CGFloat bigRadius = width/4+16;
    CGFloat centerX = screenWidth/2;
    CGFloat centerY = screenHeight/2;
    UIColor *bgcolor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:220/255.0f alpha:1.0f];

    CGRect newRect = CGRectMake(screenWidth/2-radius, screenHeight/2-radius, radius*2, radius*2);
    CGRect bigRect = CGRectMake(screenWidth/2-bigRadius, screenHeight/2-bigRadius, bigRadius*2, bigRadius*2);
    
    // Fill color.
    CGContextSetFillColorWithColor(context, bgcolor.CGColor);
    CGContextFillEllipseInRect(context, bigRect);
    
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    CGContextAddEllipseInRect(context, newRect);
    CGContextStrokePath(context);
    
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    CGContextAddEllipseInRect(context, bigRect);
    CGContextStrokePath(context);
    
    [self drawSmallArrow: context withCenterX: centerX withCenterY: centerY withRadius: radius];    
    [self drawBigArrow: context withCenterX: centerX withCenterY: centerY withRadius: radius];
    
    
    [self drawXYaxes: context withCenterX: centerX withCenterY: centerY withRadius: radius];
    [self drawNSEW: context withCenterX: centerX withCenterY: centerY withRadius: radius];
    

}

- (void) drawSmallArrow: (CGContextRef) context withCenterX: (CGFloat) centerX withCenterY: (CGFloat) centerY withRadius: (CGFloat) radius {
    CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    int count = 5;
    int outerBand = 16;
    CGPoint points[count];
    CGFloat offset = 0.3;
    
    points[0].x = (radius+outerBand)*sinf(testDirection)+centerX;
    points[0].y = -(radius+outerBand)*cosf(testDirection)+centerY;
    
    points[1].x = (radius+1)*sinf(testDirection+offset)+centerX;
    points[1].y = -(radius+1)*cosf(testDirection+offset)+centerY;
    
    points[2].x = (radius+5)*sinf(testDirection)+centerX;
    points[2].y = -(radius+5)*cosf(testDirection)+centerY;
    
    points[3].x = (radius+1)*sinf(testDirection-offset)+centerX;
    points[3].y = -(radius+1)*cosf(testDirection-offset)+centerY;
    
    points[4].x = (radius+outerBand)*sinf(testDirection)+centerX;
    points[4].y = -(radius+outerBand)*cosf(testDirection)+centerY;
    
    CGContextMoveToPoint(context, centerX, centerY);
    CGContextAddLines(context, points, count);

    CGContextSetFillColorWithColor(context, [UIColor blueColor].CGColor);
    CGContextFillPath(context);
   
}

- (void) drawBigArrow: (CGContextRef) context withCenterX: (CGFloat) centerX withCenterY: (CGFloat) centerY withRadius: (CGFloat) radius {
    CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
    int count = 5;
    int outerBand = 0;
    CGPoint points[count];
    CGFloat offset = 2.5;
    
    points[0].x = (radius+outerBand)*sinf(testDirection)+centerX;
    points[0].y = -(radius+outerBand)*cosf(testDirection)+centerY;
    
    points[1].x = (radius)*sinf(testDirection+offset)+centerX;
    points[1].y = -(radius)*cosf(testDirection+offset)+centerY;
    
    points[2].x = -(radius/2)*sinf(testDirection)+centerX;
    points[2].y = (radius/2)*cosf(testDirection)+centerY;
    
    points[3].x = (radius+1)*sinf(testDirection-offset)+centerX;
    points[3].y = -(radius+1)*cosf(testDirection-offset)+centerY;
    
    points[4].x = (radius+outerBand)*sinf(testDirection)+centerX;
    points[4].y = -(radius+outerBand)*cosf(testDirection)+centerY;
    
    CGContextMoveToPoint(context, centerX, centerY);
    CGContextAddLines(context, points, count);

    CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor);
    CGContextFillPath(context);
   
}


- (void) drawXYaxes:(CGContextRef) context withCenterX: (CGFloat) centerX withCenterY: (CGFloat) centerY withRadius: (CGFloat) radius {
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    CGFloat begin, end;
    CGFloat x, y, xx, yy;
    
    begin = 0.9; end = 1.1;
    
    x = begin*radius*sinf(testDirection)+centerX;
    y = -begin*radius*cosf(testDirection)+centerY;
    xx = end*radius*sinf(testDirection)+centerX;
    yy = -end*radius*cosf(testDirection)+centerY;
    CGContextMoveToPoint(context, x, y);
    CGContextAddLineToPoint(context, xx, yy);
    CGContextStrokePath(context);
    
    x = begin*radius*sinf(testDirection+0.5*M_PI)+centerX;
    y = -begin*radius*cosf(testDirection+0.5*M_PI)+centerY;
    xx = end*radius*sinf(testDirection+0.5*M_PI)+centerX;
    yy = -end*radius*cosf(testDirection+0.5*M_PI)+centerY;
    CGContextMoveToPoint(context,  x, y);
    CGContextAddLineToPoint(context, xx, yy);
    CGContextStrokePath(context);
    
    x = begin*radius*sinf(testDirection+M_PI)+centerX;
    y = -begin*radius*cosf(testDirection+M_PI)+centerY;
    xx = end*radius*sinf(testDirection+M_PI)+centerX;
    yy = -end*radius*cosf(testDirection+M_PI)+centerY;
    CGContextMoveToPoint(context,  x, y);
    CGContextAddLineToPoint(context, xx, yy);
    CGContextStrokePath(context);
    
    x = begin*radius*sinf(testDirection+1.5*M_PI)+centerX;
    y = -begin*radius*cosf(testDirection+1.5*M_PI)+centerY;
    xx = end*radius*sinf(testDirection+1.5*M_PI)+centerX;
    yy = -end*radius*cosf(testDirection+1.5*M_PI)+centerY;
    CGContextMoveToPoint(context,  x, y);
    CGContextAddLineToPoint(context, xx, yy);
    CGContextStrokePath(context);
}


- (void) drawNSEW:(CGContextRef) context withCenterX: (CGFloat) centerX withCenterY: (CGFloat) centerY withRadius: (CGFloat) radius {
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    CGFloat offset;
    CGFloat x, y;
    
    offset = 1.4;
    
    x = offset*radius*sinf(testDirection)+centerX;
    y = -offset*radius*cosf(testDirection)+centerY;
    [@"N" drawAtPoint:CGPointMake(x-4,y-6) withAttributes:nil];
    
    x = offset*radius*sinf(testDirection+0.5*M_PI)+centerX;
    y = -offset*radius*cosf(testDirection+0.5*M_PI)+centerY;
    [@"E" drawAtPoint:CGPointMake(x-4,y-6) withAttributes:nil];
    
    x = offset*radius*sinf(testDirection+M_PI)+centerX;
    y = -offset*radius*cosf(testDirection+M_PI)+centerY;
    [@"S" drawAtPoint:CGPointMake(x-4,y-6) withAttributes:nil];
    
    x = offset*radius*sinf(testDirection+1.5*M_PI)+centerX;
    y = -offset*radius*cosf(testDirection+1.5*M_PI)+centerY;
    [@"W" drawAtPoint:CGPointMake(x-4,y-6) withAttributes:nil];
}

@end
