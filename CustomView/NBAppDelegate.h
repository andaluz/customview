//
//  NBAppDelegate.h
//  CustomView
//
//  Created by Nordin on 27/02/14.
//  Copyright (c) 2014 Fennan Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
